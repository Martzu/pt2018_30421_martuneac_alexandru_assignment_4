package Assig4.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame{

    private JButton person = new JButton("Person");
    private JButton accounts = new JButton("Accounts");

    public View()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600,800);
        this.setVisible(true);

        c.insets = new Insets(20,20,20,20);

        panel.add(person,c);

        c.gridy = 1;
        panel.add(accounts,c);

        this.add(panel);
    }

    public void addListener(ActionListener listenForButton)
    {
        accounts.addActionListener(listenForButton);
        person.addActionListener(listenForButton);
    }

    public JButton getPerson() {
        return person;
    }

    public JButton getAccounts() {
        return accounts;
    }
}
