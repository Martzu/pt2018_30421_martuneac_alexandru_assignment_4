package Assig4.View;

import Assig4.Model.Account;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AccountView extends JFrame {


    private JLabel person = new JLabel("Enter the person:");
    private JLabel ID = new JLabel("ID:");
    private JLabel name = new JLabel("Name:");
    private JLabel address = new JLabel("Email:");

    private JTextField sID = new JTextField(7);
    private JTextField sName = new JTextField(7);
    private JTextField sAddress = new JTextField(7);
    private JTextField accountIDcheck = new JTextField(4);
    private JTextField accountIDdeposit = new JTextField(4);
    private JTextField accountIDwithdraw = new JTextField(4);
    private JTextField sumDeposit = new JTextField(7);
    private JTextField sumWithdraw = new JTextField(7);
    private JTextField newAccountS = new JTextField(7);
    private JTextField typea = new JTextField(7);
    private JTextField interest = new JTextField(7);
    private JTextField deleteAcc = new JTextField(4);

    private JButton getAccounts = new JButton("Get Accounts");
    private JButton mainMenu = new JButton("Main menu");
    private JButton deposit = new JButton("Deposit");
    private JButton withdraw = new JButton("Withdraw");
    private JButton checkAccount = new JButton("Check Account");
    private JButton createAccount = new JButton("Create Account");
    private JButton deleteAccount = new JButton("Delete Account");

    private static DefaultTableModel model;
    private JTable jTable = new JTable(model);

    private JLabel delete = new JLabel("Account number:");
    private JLabel _type = new JLabel("Type:");
    private JLabel _interest = new JLabel("Interest:");
    private JLabel accIDcheck = new JLabel("Account number:");
    private JLabel accIDdeposit = new JLabel("Account number:");
    private JLabel accIDwithdraw = new JLabel("Account number:");
    private JLabel observer =  new JLabel("");
    private JLabel message = new JLabel("");
    private JLabel sDeposit = new JLabel("Sum:");
    private JLabel sWithdraw = new JLabel("Sum:");
    private JLabel newAccountSum = new JLabel("Sum:");

    public AccountView()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000,800);
        this.setVisible(true);


        c.insets = new Insets(20,20,20,20);

        c.gridx = 0;
        panel.add(mainMenu,c);

        c.gridx = 1;
        panel.add(person,c);
        JScrollPane jScrollPane = new JScrollPane(jTable);
        if(model == null)
        {
            jScrollPane.setVisible(false);
        }
        else
        {
            jScrollPane.setVisible(true);
            this.setSize(1300,1000);
        }

        c.gridx = 2;
        panel.add(jScrollPane,c);


        c.gridy = 1;
        c.gridx = 0;
        panel.add(getAccounts,c);

        c.gridx = 1;
        panel.add(ID,c);

        c.gridx = 2;
        panel.add(sID,c);

        c.gridx = 3;
        panel.add(name,c);

        c.gridx = 4;
        panel.add(sName,c);

        c.gridx = 5;
        panel.add(address,c);

        c.gridx = 6;
        panel.add(sAddress,c);

        c.gridx = 0;
        c.gridy = 2;

        c.gridy = 3;
        panel.add(deposit,c);

        c.gridx = 1;
        panel.add(accIDdeposit,c);

        c.gridx = 2;
        panel.add(accountIDdeposit,c);

        c.gridx = 3;
        panel.add(sDeposit,c);

        c.gridx = 4;
        panel.add(sumDeposit,c);

        c.gridx = 0;

        c.gridy = 4;
        panel.add(withdraw,c);

        c.gridx = 1;
        panel.add(accIDwithdraw,c);

        c.gridx = 2;
        panel.add(accountIDwithdraw,c);

        c.gridx = 3;
        panel.add(sWithdraw,c);

        c.gridx = 4;
        panel.add(sumWithdraw,c);

        c.gridx = 0;



        c.gridy = 5;
        panel.add(createAccount,c);

        c.gridx = 1;
        panel.add(newAccountSum,c);

        c.gridx = 2;
        panel.add(newAccountS,c);

        c.gridx = 3;
        panel.add(_type,c);

        c.gridx = 4;
        panel.add(typea,c);

        c.gridx = 5;
        panel.add(_interest,c);

        c.gridx = 6;
        panel.add(interest,c);

        c.gridx = 0;



        c.gridy = 6;
        panel.add(checkAccount,c);

        c.gridx = 1;
        panel.add(accIDcheck,c);

        c.gridx = 2;
        panel.add(accountIDcheck,c);

        c.gridx= 0;

        c.gridy = 7;
        panel.add(deleteAccount,c);

        c.gridx = 1;
        panel.add(delete,c);

        c.gridx = 2;
        panel.add(deleteAcc,c);


        c.gridx = 0;

        c.gridy = 9;
        panel.add(message,c);

        c.gridy = 0;
        panel.add(observer,c);

        this.add(panel);

    }

    public void addListener(ActionListener listenForButton)
    {
        mainMenu.addActionListener(listenForButton);
        checkAccount.addActionListener(listenForButton);
        deposit.addActionListener(listenForButton);
        withdraw.addActionListener(listenForButton);
        createAccount.addActionListener(listenForButton);
        deleteAccount.addActionListener(listenForButton);
        getAccounts.addActionListener(listenForButton);
    }

    public void update(Header<Account> header)
    {
        model = new DefaultTableModel(header.getValues(),header.getParameters());
        this.jTable = new JTable(model);
    }

    public JTextField getDeleteAcc() {
        return deleteAcc;
    }

    public JButton getDeleteAccount() {
        return deleteAccount;
    }

    public JTextField getsID() {
        return sID;
    }

    public JTextField getsName() {
        return sName;
    }

    public JTextField getsAddress() {
        return sAddress;
    }

    public JButton getAccounts() {
        return getAccounts;
    }

    public JButton getMainMenu() {
        return mainMenu;
    }

    public JButton getDeposit() {
        return deposit;
    }

    public JButton getWithdraw() {
        return withdraw;
    }

    public JButton getCheckAccount() {
        return checkAccount;
    }

    public JLabel getObserver() {
        return observer;
    }

    public JLabel getMessage() {
        return message;
    }

    public JTextField getAccountIDcheck() {
        return accountIDcheck;
    }

    public JTextField getAccountIDdeposit() {
        return accountIDdeposit;
    }

    public JTextField getAccountIDwithdraw() {
        return accountIDwithdraw;
    }

    public JTextField getSumDeposit() {
        return sumDeposit;
    }

    public JTextField getSumWithdraw() {
        return sumWithdraw;
    }

    public JTextField getNewAccountS() {
        return newAccountS;
    }

    public JButton getCreateAccount() {
        return createAccount;
    }


    public JTextField getTypea() {
        return typea;
    }

    public JTextField getInterest() {
        return interest;
    }
}
