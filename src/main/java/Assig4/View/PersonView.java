package Assig4.View;

import Assig4.Model.Bank;
import Assig4.Model.Person;
import org.omg.CORBA.PERSIST_STORE;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class PersonView extends JFrame {

    private JButton addPerson = new JButton("Add Person");
    private JButton removePerson = new JButton("Remove Person");
    private JButton updatePerson = new JButton("Update Person");
    private JButton mainMenu = new JButton("Main Menu");

    private JTextField aID = new JTextField(7);
    private JTextField aName = new JTextField(7);
    private JTextField aEmail = new JTextField(7);

    private JTextField uID = new JTextField(7);
    private JTextField uName = new JTextField(7);
    private JTextField uEmail = new JTextField(7);

    private JTextField tuID = new JTextField(7);
    private JTextField tuName = new JTextField(7);
    private JTextField tuEmail = new JTextField(7);

    private JTextField dID = new JTextField(7);
    private JTextField dName = new JTextField(7);
    private JTextField dEmail = new JTextField(7);

    private JLabel addID = new JLabel("ID:");
    private JLabel addName = new JLabel("Name:");
    private JLabel addEmail = new JLabel("Email:");

    private JLabel removeID = new JLabel("ID:");
    private JLabel removeName = new JLabel("Name:");
    private JLabel removeEmail = new JLabel("Email:");

    private JLabel tupdatePerson = new JLabel("Person to update:");
    private JLabel tupdateID = new JLabel("ID:");
    private JLabel tupdateName = new JLabel("Name:");
    private JLabel tupdateEmail = new JLabel("Email:");

    private JLabel updateID = new JLabel("ID:");
    private JLabel updateName = new JLabel("Name:");
    private JLabel updateEmail = new JLabel("Email:");

    private JTable jTable;

    public PersonView()
    {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000,800);
        this.setVisible(true);

        c.insets = new Insets(10,10,10,10);

        panel.add(addPerson,c);

        c.gridx = 1;
        panel.add(addID,c);

        c.gridx = 2;
        panel.add(aID,c);

        c.gridx = 3;
        panel.add(addName,c);

        c.gridx = 4;
        panel.add(aName,c);

        c.gridx = 5;
        panel.add(addEmail,c);

        c.gridx = 6;
        panel.add(aEmail,c);



        c.gridx = 0;
        c.gridy = 1;
        panel.add(tupdatePerson,c);

        c.gridx = 1;

        panel.add(tupdateID,c);

        c.gridx = 2;
        panel.add(tuID,c);

        c.gridx = 3;
        panel.add(tupdateName,c);

        c.gridx = 4;
        panel.add(tuName,c);

        c.gridx = 5;
        panel.add(tupdateEmail,c);

        c.gridx = 6;
        panel.add(tuEmail,c);


        c.gridx = 0;
        c.gridy = 2;
        panel.add(updatePerson,c);

        c.gridx = 1;
        panel.add(updateID,c);

        c.gridx = 2;
        panel.add(uID,c);

        c.gridx = 3;
        panel.add(updateName,c);

        c.gridx = 4;
        panel.add(uName,c);

        c.gridx = 5;
        panel.add(updateEmail,c);

        c.gridx = 6;
        panel.add(uEmail,c);

        c.gridx = 0;
        c.gridy = 3;
        panel.add(removePerson,c);

        c.gridx = 1;
        panel.add(removeID,c);

        c.gridx = 2;
        panel.add(dID,c);

        c.gridx = 3;
        panel.add(removeName,c);

        c.gridx = 4;
        panel.add(dName,c);

        c.gridx = 5;
        panel.add(removeEmail,c);

        c.gridx = 6;
        panel.add(dEmail,c);

        c.gridx = 0;
        c.gridy = 4;
        panel.add(mainMenu,c);
        ArrayList<Person> persons = new ArrayList<>();
        Bank bank = new Bank();
        Person person = new Person(1,"aa","dd");
        for(Person p : bank.getPersons())
        {
            persons.add(p);
        }

        Header<Person> header = new Header<>(persons,person);

        DefaultTableModel model = new DefaultTableModel(header.getValues(),header.getParameters());
        jTable = new JTable(model);

        JScrollPane jScrollPane = new JScrollPane(jTable);

        c.gridx = 2;
        c.gridy = 5;
        panel.add(jScrollPane,c);

        this.add(panel);
    }

    public void addListener(ActionListener listenForButton)
    {
        mainMenu.addActionListener(listenForButton);
        addPerson.addActionListener(listenForButton);
        removePerson.addActionListener(listenForButton);
        updatePerson.addActionListener(listenForButton);
    }

    public JButton getAddPerson() {
        return addPerson;
    }

    public JButton getRemovePerson() {
        return removePerson;
    }

    public JButton getUpdatePerson() {
        return updatePerson;
    }

    public JButton getMainMenu() {
        return mainMenu;
    }

    public JTextField getaID() {
        return aID;
    }

    public JTextField getaName() {
        return aName;
    }

    public JTextField getaEmail() {
        return aEmail;
    }

    public JTextField getuID() {
        return uID;
    }

    public JTextField getuName() {
        return uName;
    }

    public JTextField getuEmail() {
        return uEmail;
    }

    public JTextField getdID() {
        return dID;
    }

    public JTextField getdName() {
        return dName;
    }

    public JTextField getdEmail() {
        return dEmail;
    }

    public JTextField getTuID() {
        return tuID;
    }

    public JTextField getTuName() {
        return tuName;
    }

    public JTextField getTuEmail() {
        return tuEmail;
    }
}
