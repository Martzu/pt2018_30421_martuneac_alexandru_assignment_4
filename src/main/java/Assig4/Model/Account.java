package Assig4.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


public class Account extends Observable implements Serializable{
    private double sum;
    private double interest;
    private boolean savingAccount;
    private int numberOfOperations;
    private ArrayList<Observer> observers = new ArrayList<>();

    public Account(boolean savingAccount, int numberOfOperations, double sum, double interest)
    {
        super();
        this.savingAccount = savingAccount;
        if(savingAccount)
        {
            this.numberOfOperations = numberOfOperations;
            this.interest = interest;
        }
        this.sum = sum;

    }

    public Account()
    {

    }

    public void notifyObservers(Observable observable, Double sum)
    {
        for(Observer o : observers)
        {
            o.update(observable,this.sum);
        }
    }

    public void registerObserver(Observer observer)
    {
        observers.add(observer);
    }

    public void removeObserver(Observer observer)
    {
        observers.remove(observer);
    }

    public String deposit(double sum)
    {
        String message = "";
        if(savingAccount == true && numberOfOperations != 0)
        {

            numberOfOperations--;
            this.sum += sum;
            message = "Successful";
            setChanged();
            notifyObservers(this,sum);
            return message;

        }
        if(savingAccount == false)
        {
            this.sum += sum;
            message = "Successful";
            setChanged();
            notifyObservers(this,sum);
            return message;
        }
        message = "No more operations allowed";
        return message;
    }



    public double checkAccount()
    {
        if(savingAccount == true)
        {
            return sum + sum*interest;
        }
        return sum;

    }

    public String withdraw(double sum)
    {
        String message = "";
        if(savingAccount == true && numberOfOperations != 0)
        {
            if(sum <= this.sum)
            {
                this.sum -= sum;
                numberOfOperations--;
                setChanged();
                notifyObservers(this,sum);
                message = "Sucessful";
            }
            else
            {
                message = "Not enough money in the account";
            }
            return message;
        }
        if(savingAccount == false)
        {
            if(sum <= this.sum)
            {
                this.sum -= sum;
                setChanged();
                notifyObservers(this,sum);
                message = "Successful";
            }
            else
            {
                message = "Not enough money in the account";
            }
            return message;
        }
        message = "No more operations allowed";
        return message;
    }


    public ArrayList<Observer> getObservers() {
        return observers;
    }

    public void setObservers(ArrayList<Observer> observers) {
        this.observers = observers;
    }

    public double getSum() {
        return sum;
    }

    public double getInterest() {
        return interest;
    }

    public int getNumberOfOperations() {
        return numberOfOperations;
    }

    public boolean isSavingAccount() {
        return savingAccount;
    }


}
