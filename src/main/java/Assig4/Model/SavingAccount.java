package Assig4.Model;

import Assig4.Model.Account;

import java.io.Serializable;

public class SavingAccount extends Account implements Serializable {

    public SavingAccount(double sum, double interest)
    {
        super(true,1,sum,interest);
    }


}
