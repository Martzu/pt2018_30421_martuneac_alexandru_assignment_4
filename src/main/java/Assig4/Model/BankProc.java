package Assig4.Model;

import java.util.ArrayList;

public interface BankProc {

    /**
     * @precondition an user is introduced
     * @postcondition person is added
     * @param person
     */

    void addPerson(Person person);


    /**
     * @precondition user exists
     * @postcondition user is removed
     * @param person
     */
    void removePerson(Person person);


    /**
     * @precondition person and account exist
     * @postcondition account is removed
     * @param person
     * @param i
     * @return
     */
    String removeAccount(Person person, int i);

    /**
     * @precondition person and account exist
     * @postcondition account is returned
     * @param person
     * @param i
     * @return String
     */

    Account getAccount(Person person, int i);

    /**
     * @precondition person and account exist
     * @postcondition returns a string of the existing sum
     * @invariant account user
     * @param person
     * @param i
     * @return Account
     */

    String checkAccount(Person person, int i);

    /**
     * @precondition person exists
     * @postcondition account is created
     * @invariant account user
     * @param person
     * @param account
     * @return String
     */

    void addAccount(Person person, Account account);

    /**
     * @precondition person exists
     * @postcondition all accounts are returned
     * @invariant account user
     * @param person
     * @return ArrayList<Account></Account>
     */

    ArrayList<Account> getAccounts(Person person);

    /**
     * @precondition person and account exists
     * @postcondition account is saving, allows only one withdraw, account is spending, allows multiple
     * @invariant account user
     * @param person
     * @param i
     * @param sum
     */
    String deposit(Person person, int i, double sum);


    /**
     * @precondition account and user exist
     * @postcondition sum is withdrawned
     * @invariant person, account ownership
     * @param person
     * @param i
     * @param sum
     * @return
     */
    String withdraw(Person person, int i, double sum);

}
