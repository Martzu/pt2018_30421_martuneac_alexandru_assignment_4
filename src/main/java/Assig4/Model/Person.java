package Assig4.Model;



import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable, Observer
{

    int id;
    private String name;
    private String email;

    public Person(int id, String name, String email)
    {
        this.id = id;
        this.name = name;
        this.email = email;
    }


    @Override
    public boolean equals(Object o) {
        return ((Person) o).id == this.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public int getId() {
        return id;
    }


    @Override
    public void update(Observable observable, Object o) {
        System.out.println("One of person's " + id + " account sum has changed");
    }
}
