package Assig4.Model;

import java.io.*;
import java.util.*;

public class Bank implements BankProc, Serializable
{

    FileOutputStream fileOutputStream;
    FileInputStream fileInputStream;
    ObjectInputStream in;
    ObjectOutputStream out;


    public Set<Person> getPersons()
    {
        return this.hashMap.keySet();
    }

    public HashMap<Person, ArrayList<Account>> getHashMap() {

        deserialize();
        return hashMap;
    }//just to debug

    HashMap<Person,ArrayList<Account>> hashMap;
    public Bank()
    {
        deserialize();
        if(hashMap == null)
        {
            hashMap = new HashMap<Person,ArrayList<Account>>();
        }

    }

    private void serialize()
    {
        try {
            fileOutputStream = new FileOutputStream("data.ser");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            out = new ObjectOutputStream(fileOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.writeObject(this.hashMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deserialize()
    {
        try {
            fileInputStream = new FileInputStream("data.ser");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            in = new ObjectInputStream(fileInputStream);
        } catch (IOException e) {
            //e.printStackTrace();
        }
        try {
            this.hashMap = (HashMap<Person,ArrayList<Account>>) in.readObject();
        } catch (IOException e) {
            //e.printStackTrace();
        } catch (ClassNotFoundException e) {
            //e.printStackTrace();
        }catch (NullPointerException e)
        {

        }
        try {
            in.close();
        } catch (IOException e) {
            //e.printStackTrace();
        } catch (NullPointerException e) {

        }

    }

    @Override
    public void addPerson(Person person)
    {
        ArrayList<Account> accounts = new ArrayList<>();
        hashMap.put(person,accounts);
        serialize();

    }

    public void addAccount(Person person, Account account)
    {
        assert (hashMap.get(person) != null);
        account.registerObserver(person);
        hashMap.get(person).add(account);
        serialize();

    }

    @Override
    public void removePerson(Person person)
    {
        assert (hashMap.get(person) != null);
        hashMap.remove(person);
        serialize();
    }

    @Override
    public String removeAccount(Person person, int i)
    {
        assert (hashMap.get(person) != null);
        assert (hashMap.get(person).size() > i);

        String message = "";
        int toDelete = 0;
        Iterator<Account> iterator = hashMap.get(person).iterator();
        while(iterator.hasNext())
        {
            Account a = iterator.next();
            if(toDelete == i)
            {
                a.removeObserver(person);
                iterator.remove();
                message = "Account deleted";
                serialize();
                return message;
            }
            toDelete++;
        }
        message = "Account doesn't exist";
        serialize();
        return message;
    }

    public String checkAccount(Person person, int i)
    {
        String message = "";
        int index = 0;
        for(Account a : hashMap.get(person))
        {
            if(i == index)
            {
                message = Double.toString(a.checkAccount());
                return  message;
            }
            index++;
        }
        message = "Account doesn't exist";
        return message;
    }

    public ArrayList<Account> getAccounts(Person person)
    {
        return hashMap.get(person);
    }

    public Account getAccount(Person person, int i)
    {
        return hashMap.get(person).get(i);
    }


    public String withdraw(Person person, int i, double sum)
    {
        String message = "";
        if(hashMap.get(person).get(i).isSavingAccount() == true)
        {
            assert (hashMap.get(person).get(i).getNumberOfOperations() != 0);
            assert (hashMap.get(person).get(i).checkAccount() >= sum);
            message = hashMap.get(person).get(i).withdraw(sum);
        }
        else
        {
            assert (hashMap.get(person).get(i).getSum() >= sum);
            message = hashMap.get(person).get(i).withdraw(sum);
        }
        serialize();
        return message;
    }

    public String deposit(Person person, int i, double sum)
    {
        String message = "";
        if(hashMap.get(person).get(i).isSavingAccount() == true)
        {
            assert (hashMap.get(person).get(i).getNumberOfOperations() != 0);
            message = hashMap.get(person).get(i).deposit(sum);
        }
        else
        {
            assert(hashMap.get(person).get(i) != null);
            message = hashMap.get(person).get(i).deposit(sum);
        }
        serialize();
        return message;
    }



}
