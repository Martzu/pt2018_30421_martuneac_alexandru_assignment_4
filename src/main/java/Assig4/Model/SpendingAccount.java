package Assig4.Model;

import Assig4.Model.Account;

import java.io.Serializable;

public class SpendingAccount extends Account implements Serializable {

    public SpendingAccount(double sum)
    {
        super(false,0,sum,0);
    }
}
