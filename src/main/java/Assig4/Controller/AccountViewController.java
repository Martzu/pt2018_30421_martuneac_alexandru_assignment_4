package Assig4.Controller;


import Assig4.Model.*;
import Assig4.View.AccountView;
import Assig4.View.Header;
import Assig4.View.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AccountViewController {
    AccountView view;

    public AccountViewController(AccountView view)
    {
        this.view = view;
        this.view.addListener(new Listener());
    }

    class Listener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == view.getMainMenu())
            {
                view.dispose();
                View view1 = new View();
                Controller controller = new Controller(view1);
                view1.setVisible(true);
            }
            if(arg0.getSource() == view.getCheckAccount())
            {
                Bank bank = new Bank();
                Person p = new Person(Integer.parseInt(view.getsID().getText()), view.getsName().getText(), view.getsAddress().getText());
                view.getMessage().setText(bank.checkAccount(p,Integer.parseInt(view.getAccountIDcheck().getText()) - 1));
            }

            if(arg0.getSource() == view.getDeposit())
            {
                Bank bank = new Bank();
                Person p = new Person(Integer.parseInt(view.getsID().getText()), view.getsName().getText(), view.getsAddress().getText());
                view.getMessage().setText(bank.deposit(p,Integer.parseInt(view.getAccountIDdeposit().getText())-1,Double.parseDouble(view.getSumDeposit().getText())));
            }

            if(arg0.getSource() == view.getWithdraw())
            {
                Bank bank = new Bank();
                Person p = new Person(Integer.parseInt(view.getsID().getText()), view.getsName().getText(), view.getsAddress().getText());
                view.getMessage().setText(bank.withdraw(p,Integer.parseInt(view.getAccountIDwithdraw().getText())-1,Double.parseDouble(view.getSumWithdraw().getText())));
            }

            if(arg0.getSource() == view.getCreateAccount())
            {
                Bank bank = new Bank();
                Person p = new Person(Integer.parseInt(view.getsID().getText()), view.getsName().getText(), view.getsAddress().getText());
                if(view.getTypea().getText().equals("Saving"))
                {
                    if(Double.parseDouble(view.getNewAccountS().getText()) < 1000)
                    {
                        view.getMessage().setText("At least 1000 is required");
                    }
                    else
                    {
                        view.getMessage().setText("Suscessful");
                    }
                    Account account = new SavingAccount(Double.parseDouble(view.getNewAccountS().getText()),Double.parseDouble(view.getInterest().getText()));
                    bank.addAccount(p,account);
                }
                else
                {
                    Account account = new SpendingAccount(Double.parseDouble(view.getNewAccountS().getText()));
                    bank.addAccount(p,account);
                    view.getMessage().setText("Successful");
                }
            }

            if(arg0.getSource() == view.getDeleteAccount())
            {
                Bank bank = new Bank();
                Person p = new Person(Integer.parseInt(view.getsID().getText()), view.getsName().getText(), view.getsAddress().getText());
                view.getMessage().setText(bank.removeAccount(p,Integer.parseInt(view.getDeleteAcc().getText())-1));
            }

            if(arg0.getSource() == view.getAccounts())
            {
                Bank bank = new Bank();
                Person p = new Person(Integer.parseInt(view.getsID().getText()), view.getsName().getText(), view.getsAddress().getText());
                Header<Account> header = new Header<>(bank.getAccounts(p),new Account());
                view.update(header);
                view.dispose();
                AccountView view1 = new AccountView();
                AccountViewController controller1 = new AccountViewController(view1);
                view1.setVisible(true);
            }
        }
    }
}
