package Assig4.Controller;

import Assig4.View.AccountView;
import Assig4.View.PersonView;
import Assig4.View.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    View view;

    public Controller(View view)
    {
        this.view = view;
        this.view.addListener(new Listener());
    }

    class Listener implements ActionListener
    {
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == view.getAccounts())
            {
                view.dispose();
                AccountView view1 = new AccountView();
                AccountViewController controller1 = new AccountViewController(view1);
                view1.setVisible(true);
            }
            if(arg0.getSource() == view.getPerson())
            {
                view.dispose();
                PersonView view2 = new PersonView();
                PersonViewController controller2 = new PersonViewController(view2);
                view2.setVisible(true);

            }
        }
    }

}
