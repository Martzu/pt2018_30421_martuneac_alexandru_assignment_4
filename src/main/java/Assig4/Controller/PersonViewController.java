package Assig4.Controller;

import Assig4.Model.Account;
import Assig4.Model.Bank;
import Assig4.Model.Person;
import Assig4.View.PersonView;
import Assig4.View.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class PersonViewController {
    PersonView view;

    public PersonViewController(PersonView view)
    {
        this.view = view;
        this.view.addListener(new Listener());
    }

    class Listener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent arg0)
        {
            if(arg0.getSource() == view.getAddPerson())
            {
                Person person = new Person(Integer.parseInt(view.getaID().getText()),view.getaName().getText(),view.getaEmail().getText());
                Bank bank = new Bank();
                bank.addPerson(person);
            }
            if(arg0.getSource() == view.getUpdatePerson())
            {
                Person person = new Person(Integer.parseInt(view.getTuID().getText()),view.getTuName().getText(),view.getTuEmail().getText());

                Bank bank = new Bank();

                ArrayList<Account> accounts = bank.getAccounts(person);

                Person person1 = new Person(Integer.parseInt(view.getuID().getText()),view.getuName().getText(),view.getuEmail().getText());

                bank.removePerson(person);
                bank.addPerson(person1);
                for(Account a : accounts)
                {
                    bank.addAccount(person1,a);
                }

            }
            if(arg0.getSource() == view.getRemovePerson())
            {
                Person person = new Person(Integer.parseInt(view.getdID().getText()),view.getdName().getText(),view.getdEmail().getText());
                Bank bank = new Bank();
                bank.removePerson(person);
            }

            if(arg0.getSource() == view.getMainMenu())
            {
                view.dispose();
                View view1 = new View();
                Controller controller1 = new Controller(view1);
                view1.setVisible(true);
            }
        }
    }
}
