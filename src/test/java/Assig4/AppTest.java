package Assig4;

import static org.junit.Assert.assertTrue;

import Assig4.Model.*;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void checkAccountTest()
    {
        Person person = new Person(7,"Test","Test@email.com");
        Account account = new SpendingAccount(400);
        Bank bank = new Bank();
        bank.addPerson(person);
        bank.addAccount(person,account);
        assertTrue(bank.getAccount(person,0) != null);
    }

    @Test
    public void getAccountsTest()
    {
        Person person = new Person(7,"Test","Test@email.com");
        Account account = new SpendingAccount(400);
        Account account1 = new SpendingAccount(300);
        Account account2 = new SavingAccount(600,0.2);
        Bank bank = new Bank();
        bank.addPerson(person);
        bank.addAccount(person,account);
        bank.addAccount(person,account1);
        bank.addAccount(person,account2);
        assertTrue(bank.getAccounts(person) != null);
    }

    @Test
    public void withdrawSpendingTest()
    {
        Person person = new Person(7,"Test","Test@email.com");
        Account account = new SpendingAccount(400);
        Bank bank = new Bank();
        bank.addPerson(person);
        bank.addAccount(person,account);
        bank.withdraw(person,0,30);
        assertTrue(account.checkAccount() == 370);
    }

    @Test
    public void withdrawSavingTest()
    {
        Person person = new Person(7,"Test","Test@email.com");
        Account account = new SavingAccount(400,0.1);
        Bank bank = new Bank();
        bank.addPerson(person);
        bank.addAccount(person,account);
        bank.withdraw(person,0,30);
        assertTrue(Double.parseDouble(bank.checkAccount(person,0)) == 407.0 && bank.getAccount(person,0).getNumberOfOperations() == 0);


    }

    @Test
    public void depositSpendingTest()
    {
        Person person = new Person(7,"Test","Test@email.com");
        Account account = new SpendingAccount(400);
        Bank bank = new Bank();
        bank.addPerson(person);
        bank.addAccount(person,account);
        bank.deposit(person,0,30);
        assertTrue(account.checkAccount() == 430);
    }

    @Test
    public void depositSavingTest()
    {
        Person person = new Person(7,"Test","Test@email.com");
        Account account = new SavingAccount(400,0.1);
        Bank bank = new Bank();
        bank.addPerson(person);
        bank.addAccount(person,account);
        bank.deposit(person,0,40);
        assertTrue(account.checkAccount() == 484);
    }

}
